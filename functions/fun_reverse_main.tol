
//////////////////////////////////////////////////////////////////////////////
Set ObtainSheet.Main(NameBlock object) 
//////////////////////////////////////////////////////////////////////////////
{
  Set main_header = [["module",	"attribute",	"value"]];
  Set main = [[
    [["general", "specification", "2.2"]],
    [["general", "name", object::GetName(?)]],
    [["general", "version", object::GetVersion(?)]]
  ]] << {
    Text description = object::GetDescription(?);
    If(TextLength(description), [[
      [["general", "description", description]]
    ]], Empty)
  };
  Text objSubclass = object::GetSubclass(?);
  NameBlock dataSet = Case(objSubclass=="DataSet", object,
   objSubclass=="Model", object::GetDataSet(?),
   objSubclass=="Estimation", object::GetModel(?)::GetDataSet(?),
   objSubclass=="Forecast", object::GetModel(?)::GetDataSet(?));
  Set mainD = ObtainSheet.Main_DataSet(dataSet);
  Set mainE = If(objSubclass=="Estimation", 
    ObtainSheet.Main_Estimation(object), Empty);
  Set mainF = If(objSubclass=="Forecast", 
    ObtainSheet.Main_Forecast(object), Empty);
  main<<mainD<<mainE<<mainF
};

//////////////////////////////////////////////////////////////////////////////
Set ObtainSheet.Main_DataSet(NameBlock dataSet) 
//////////////////////////////////////////////////////////////////////////////
{
  Set intervals = dataSet::GetIntervals(?);
  Set mainI = EvalSet(intervals, Set (NameBlock interval) {
    Text name = interval::GetName(?);
    [["dataSet", "interval."<<name, name<<", "<<interval::GetBegin(?)
      <<", "<<interval::GetEnd(?)]]
  });
  mainI
};

//////////////////////////////////////////////////////////////////////////////
Set ObtainSheet.Main_Estimation(NameBlock estimation) 
//////////////////////////////////////////////////////////////////////////////
{
  NameBlock settings = estimation::GetStrategy(?)::GetSettings(?);
  Set main1 = [[
    [["estimation", "strategy", settings::GetSubclass(?)]]
  ]];
  Text class = ClassOf(settings);
  NameBlock default = Eval(class<<"::Default(?)");
  Set members = Select(Members(settings), Real (Set member) {
    member->Type <: [["Real","Text"]] & member->Access <: [["Public", "Read only"]]
  });
  Set main2 = SetConcat(EvalSet(members, Set (Set member) {
    Anything s1 = Eval("settings::"<<member->Name);
    Anything s2 = Eval("default::"<<member->Name);
    If(s1==s2, Empty, [[
      [["estimation", member->Name, s1]]
    ]])
  }));
  main1<<main2
};

//////////////////////////////////////////////////////////////////////////////
Set ObtainSheet.Main_Forecast(NameBlock forecast) 
//////////////////////////////////////////////////////////////////////////////
{
  NameBlock settings = forecast::GetSettings(?);
  Set main1 = [[
    [["forecast", "type", settings::GetSetting("_.mode")]]
  ]];
  Text class = ClassOf(settings);
  NameBlock default = Eval(class<<"::Default(?)");
  Set members = Select(Members(settings), Real (Set member) {
    member->Type <: [["Real","Text"]] & member->Access <: [["Public", "Read only"]]
  });
  Set main2 = SetConcat(EvalSet(members, Set (Set member) {
    Anything s1 = Eval("settings::"<<member->Name);
    Anything s2 = Eval("default::"<<member->Name);
    If(""<<s1==""<<s2, Empty, [[
      [["forecast", member->Name, s1]]
    ]])
  }));
  main1<<main2
};
